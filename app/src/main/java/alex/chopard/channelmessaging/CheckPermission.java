package alex.chopard.channelmessaging;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

import static android.support.v4.content.PermissionChecker.checkCallingOrSelfPermission;

/**
 * Created by AlexChop on 05/02/2018.
 */

public class CheckPermission {
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 2;
    private static final int MY_PERMISSIONS_REQUEST_INTERNET = 3;
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_NETWORK_STATE = 4;
    private static final int ALL_PERMISSONS = 99;


    public static void checkAllPermission(Activity activity) {
        ActivityCompat.requestPermissions(activity, new String[] {
                Manifest.permission.INTERNET,
                Manifest.permission.ACCESS_NETWORK_STATE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.RECORD_AUDIO}, ALL_PERMISSONS);
    }

    public static void checkPermissionReadExternalStorage(Activity activity, Context context) {
        if(!ReadExternalStorageIsGaranted(context))
            ActivityCompat.requestPermissions(activity, new String[] {Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
    }

    public static void checkPermissionWriteExternalStorage(Activity activity, Context context) {
        if(!WriteExternalStorageIsGaranted(context))
            ActivityCompat.requestPermissions(activity, new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
    }



    public static boolean ReadExternalStorageIsGaranted(Context context) {
        return checkCallingOrSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean WriteExternalStorageIsGaranted(Context context) {
        return checkCallingOrSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

}
