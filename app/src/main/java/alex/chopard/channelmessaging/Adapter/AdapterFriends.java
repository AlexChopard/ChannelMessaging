package alex.chopard.channelmessaging.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import alex.chopard.channelmessaging.R;

/**
 * Created by AlexChop on 05/02/2018.
 */

public class AdapterFriends extends ArrayAdapter<HashMap<String, String>> {


    private final Context context;
    private final List<HashMap<String, String>> values;

    public AdapterFriends(Context context, List<HashMap<String, String>> values){
        super(context, R.layout.activity_listamis, values);
        this.context = context;
        this.values = values;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.rowfriends, parent, false);


        TextView friendName = rowView.findViewById(R.id.tv_friend_name);
        TextView idUser = rowView.findViewById(R.id.tv_idUser);

        friendName.setText(values.get(position).get("name"));
        idUser.setText(values.get(position).get("id"));

        return rowView;
    }

}
