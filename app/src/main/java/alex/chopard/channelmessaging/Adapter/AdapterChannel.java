package alex.chopard.channelmessaging.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import alex.chopard.channelmessaging.R;

/**
 * Created by AlexChop on 21/01/2018.
 */

public class AdapterChannel extends ArrayAdapter<HashMap<String, String>> {

    private final Context context;
    private final List<HashMap<String, String>> values;

    public AdapterChannel(Context context, List<HashMap<String, String>> values){
        super(context, R.layout.activity_channellist, values);
        this.context = context;
        this.values = values;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.rowlayout, parent, false);

        rowView.setId(Integer.parseInt(values.get(position).get("id")));

        TextView channelName = rowView.findViewById(R.id.tv_channel_name);
        TextView channelNumber = rowView.findViewById(R.id.tv_channel_number);

        channelName.setText(values.get(position).get("name"));
        channelNumber.setText(values.get(position).get("number"));

        return rowView;
    }
}
