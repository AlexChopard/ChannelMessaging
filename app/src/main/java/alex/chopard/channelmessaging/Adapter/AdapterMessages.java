package alex.chopard.channelmessaging.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import alex.chopard.channelmessaging.AsyncTask.LoadPicture;
import alex.chopard.channelmessaging.AsyncTask.LoadSound;
import alex.chopard.channelmessaging.Interface.OnDownloadListener;
import alex.chopard.channelmessaging.R;

import static alex.chopard.channelmessaging.Activity.ChannelActivity.fileDir;
import static alex.chopard.channelmessaging.AsyncTask.LoadPicture.pictureFormat;
import static alex.chopard.channelmessaging.AsyncTask.LoadSound.soundFormat;

/**
 * Created by AlexChop on 21/01/2018.
 */

public class AdapterMessages extends ArrayAdapter<HashMap<String, String>> implements OnDownloadListener{

    private final Context context;
    private final List<HashMap<String, String>> values;
    private ImageView photo, iv_messageImageUrl;
    private String userName, messageImageName;
    private MediaPlayer mediaPlayer;

    public AdapterMessages(Context context, List<HashMap<String, String>> values){
        super(context, R.layout.activity_channellist, values);
        this.context = context;
        this.values = values;

    }

    @NonNull
    @Override
    public View getView(int position, View convertView,@NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        String sendbyme = values.get(position).get("sendbyme");
        String everRead = values.get(position).get("everRead");
        String messageImageUrl = values.get(position).get("messageImageUrl");
        String soundUrl = values.get(position).get("soundUrl");
        messageImageName = "";
        View rowView;

        if(messageImageUrl == null || messageImageUrl.equals("")){ // Si on est dans un message privée
            if(soundUrl == null || soundUrl.equals("")){ // Si il y a pas de son
                if(sendbyme != null && sendbyme.equals("1")) // Si c'est mon message
                    rowView = inflater.inflate(R.layout.rowmessageme, parent, false);
                else // Si c'est l'autre qui à envoyé le message ou qu'on est dans un channel
                    rowView = inflater.inflate(R.layout.rowmessage, parent, false);
            }else{ // Si il y a un son
                rowView = inflater.inflate(R.layout.rowmessageson, parent, false);
                final ImageView iv_playstop = rowView.findViewById(R.id.iv_playstop);
                final ProgressBar progress = rowView.findViewById(R.id.pb_son);

                String messageSonName = soundUrl.substring(soundUrl.lastIndexOf("/") + 1, soundUrl.length() - soundFormat.length());
                final File file = new File(fileDir + "/" + messageSonName + soundFormat);
                if(!file.exists()){
                    LoadSound loadSon = new LoadSound();
                    loadSon.addOnDownloadListener(this);

                    loadSon.execute(messageSonName, soundUrl);
                }

                iv_playstop.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try{
                            if(file.exists()){ // Le fichier à bien été télécharger
                                if(mediaPlayer == null || !mediaPlayer.isPlaying()){ // Jouer le son

                                    if(mediaPlayer == null){
                                        mediaPlayer = new MediaPlayer();
                                        mediaPlayer.setDataSource(file.getPath());
                                        mediaPlayer.prepare();
                                        progress.setMax(mediaPlayer.getDuration());
                                    }

                                    mediaPlayer.start();

                                    /*** PROGRESS BAR ***/
                                    ScheduledExecutorService service = Executors.newScheduledThreadPool(1);
                                    service.scheduleWithFixedDelay(new Runnable()
                                    {
                                        @Override
                                        public void run()
                                        {
                                            int currentPosition = mediaPlayer.getCurrentPosition();
                                            progress.setProgress(currentPosition);
                                            if(currentPosition >= mediaPlayer.getDuration() - 0.5){ // Quand ona arrive à la fin
                                                progress.setProgress(0);                                                iv_playstop.setImageResource(R.drawable.quantum_ic_play_arrow_grey600_48);
                                                iv_playstop.setImageResource(R.drawable.quantum_ic_play_arrow_grey600_48);
                                                mediaPlayer.release();
                                                mediaPlayer = null;
                                            }
                                        }
                                    }, 1, 1, TimeUnit.MICROSECONDS);
                                    /*** END ***/

                                    iv_playstop.setImageResource(R.drawable.cast_ic_mini_controller_pause_large);
                                } else{ // Stoper le son
                                    mediaPlayer.pause();
                                    iv_playstop.setImageResource(R.drawable.quantum_ic_play_arrow_grey600_48);
                                }

                            }
                        }catch (Exception e){
                            Log.i("error", e.getMessage());
                        }
                    }
                });



            }
        }else{ // Si le message contient une image
            rowView = inflater.inflate(R.layout.rowmessageimage, parent, false);

            messageImageName = messageImageUrl.substring(messageImageUrl.lastIndexOf("/") + 1, messageImageUrl.length() - pictureFormat.length());

            iv_messageImageUrl = rowView.findViewById(R.id.iv_messageimage);
            if(!setPicture(iv_messageImageUrl, messageImageName, false)){
                LoadPicture loadPicture = new LoadPicture();
                loadPicture.addOnDownloadListener(this);

                loadPicture.execute(messageImageName, messageImageUrl);
            }
        }


        TextView username = rowView.findViewById(R.id.tv_username);
        TextView message = rowView.findViewById(R.id.tv_message);
        TextView date = rowView.findViewById(R.id.tv_date);
        photo = rowView.findViewById(R.id.im_userPhoto);

        userName = values.get(position).get("userName");

        username.setText(userName);
        String sMessage = values.get(position).get("message");
        try {
            // On encode correctement le texte en UTF-8
            message.setText(new String(sMessage.getBytes("iso-8859-1"), "utf8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        date.setText(values.get(position).get("date"));

        if(everRead != null && everRead.equals("0")){
            message.setTypeface(message.getTypeface(), Typeface.BOLD);
        }

        // Trop gourement en ressource quand y a pas d'image à charger !!!
        if(!setPicture(photo, userName, true)){
            /*LoadPicture loadPicture = new LoadPicture();
            loadPicture.addOnDownloadListener(this);

            loadPicture.execute(userName, values.get(position).get("imageURL"));*/
        }

        return rowView;
    }

    @Override
    public void onDownloadComplete(String res) {
        if(res.equals("OK")) {
            setPicture(photo, userName, true);
            if(!messageImageName.equals(""))
                setPicture(iv_messageImageUrl, messageImageName, false);
        }
    }

    @Override
    public void onDownloadError(String error) {
        if(error.equals("!OK")) {
            Log.i("err", "erreur de chargement de l'image");
        }
    }


    private boolean setPicture(ImageView iv, String fileName, Boolean rounded){
        try{
            File file = new File(fileDir + "/" + fileName + pictureFormat);
            if(file.exists()){ // Si l'image est déjà enregister sur le téléphone
                Bitmap bm = BitmapFactory.decodeFile(file.getPath());

                if(rounded)
                    bm = getRoundedCornerBitmap(bm);

                iv.setImageBitmap(bm);

                return true;
            }else{ // Si l'image existe pas, y va falloir le télécharger
                return false;
            }

        }catch (Exception ex){
            Log.i("erreur", ex.getMessage());
            return false;
        }
    }

    /**** https://ruibm.com/2009/06/16/rounded-corner-bitmaps-on-android/ ****/
    private static Bitmap getRoundedCornerBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = 10000;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }
}
