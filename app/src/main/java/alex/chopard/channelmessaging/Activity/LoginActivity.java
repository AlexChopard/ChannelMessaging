package alex.chopard.channelmessaging.Activity;

import android.animation.Animator;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Random;

import alex.chopard.channelmessaging.CheckPermission;
import alex.chopard.channelmessaging.Class.ResponseLogin;
import alex.chopard.channelmessaging.AsyncTask.HttpPostHandler;
import alex.chopard.channelmessaging.Interface.OnDownloadListener;
import alex.chopard.channelmessaging.R;

import static alex.chopard.channelmessaging.MyFirebaseInstanceIDService.PREF_TOKEN_APP;

public class LoginActivity extends Activity implements View.OnClickListener, OnDownloadListener {

    public static final String PREF_TOKEN = "MyAccessToken";

    EditText identifiant, motdepasse;
    TextView tv_randome_message;
    Button valider;
    Gson gson;
    SharedPreferences setting;
    ImageView iv_logo;
    Handler mHandlerTada;

    int mShortDelay;

    private static final String[] explainStringArray = {
            "Connecte toi pour chatter avec tes amis",
            "Sloubi 1",
            "Pas changer assiette pour fromaage !"
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        gson = new Gson();

        SharedPreferences setting = getSharedPreferences(PREF_TOKEN_APP, 0);
        SharedPreferences.Editor editor = setting.edit();
        editor.putString("apptoken", FirebaseInstanceId.getInstance().getToken());
        editor.apply();
        editor.commit();

        CheckPermission.checkAllPermission(this);

        identifiant = findViewById(R.id.et_identifiant);
        motdepasse = findViewById(R.id.et_motdepasse);
        valider = findViewById(R.id.btn_valider);
        iv_logo = findViewById(R.id.iv_logo);
        tv_randome_message = findViewById(R.id.tv_randome_message);

        valider.setOnClickListener(this);
        iv_logo.setOnClickListener(this);

        mHandlerTada = new Handler(); // android.os.handler
        mShortDelay = 4000; //milliseconds

        // Animation logo
        mHandlerTada.postDelayed(new Runnable(){
            public void run(){
                YoYo.with(Techniques.Tada)
                        .duration(700)
                        .repeat(1)
                        .playOn(iv_logo);


                mHandlerTada.postDelayed(this, mShortDelay);
            }
        }, mShortDelay);

        // Défilement text randome
        mHandlerTada.postDelayed(new Runnable(){
            public void run(){
                // On met un autre texte aléatoire
                tv_randome_message.setText(explainStringArray[new Random().nextInt(explainStringArray.length)]);

                YoYo.with(Techniques.SlideInLeft)
                        .duration(750)
                        .withListener(new Animator.AnimatorListener() {

                            @Override
                            public void onAnimationStart(Animator animation) {

                            }
                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }
                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                YoYo.with(Techniques.SlideOutRight)
                                        .duration(750)
                                        .delay(2000)
                                        .playOn(tv_randome_message);
                            }
                        }).playOn(tv_randome_message);


                mHandlerTada.postDelayed(this, 3500);
            }
        }, 2000);

    }

    @Override
    protected void onResume() {
        super.onResume();
        valider.clearAnimation();
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == valider.getId() || view.getId() == R.id.snackbar_action){
            String id = identifiant.getText().toString();
            String mdp = motdepasse.getText().toString();

            // Début de l'annimation du bouton
            Animation animSlideLeft = AnimationUtils.loadAnimation(this, R.anim.slide_left_right);
            valider.startAnimation(animSlideLeft);

            setting = getSharedPreferences(PREF_TOKEN_APP, 0);
            String apptoken = setting.getString("apptoken", "null");

            HashMap<String, String > hm = new HashMap<>();
            hm.put("function", "connect");
            hm.put("username", id);
            hm.put("password", mdp);
            if(!apptoken.equals("null"))
                hm.put("registrationid", apptoken);

            HttpPostHandler requet = new HttpPostHandler();

            requet.addOnDownloadListener(this);
            requet.execute(hm);

        }else if(view.getId() == R.id.iv_logo){
            YoYo.with(Techniques.Tada)
                    .duration(700)
                    .repeat(1)
                    .playOn(iv_logo);
        }
    }

    @Override
    public void onDownloadComplete(String res) {
        //Log.i("res", res);
        ResponseLogin reponse = gson.fromJson(res, ResponseLogin.class);
        if(reponse.getCode().equals("200")){
            SharedPreferences setting = getSharedPreferences(PREF_TOKEN, 0);
            SharedPreferences.Editor editor = setting.edit();
            editor.putString("accesstoken", reponse.getAccesstoken());
            editor.apply();
            editor.commit();

            Intent intent = new Intent(LoginActivity.this, ChannelListActivity.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(LoginActivity.this, iv_logo, "logo").toBundle());
            } else{
                startActivity(intent);
            }
        } else{
            valider.clearAnimation();
            Snackbar mySnackbar = Snackbar.make(findViewById(R.id.ll_main),
                    R.string.error_connect, Snackbar.LENGTH_INDEFINITE);
            mySnackbar.setAction(R.string.error_retry, this);
            mySnackbar.show();
        }
    }

    @Override
    public void onDownloadError(String error) {
        Log.i("error", error);
    }

}
