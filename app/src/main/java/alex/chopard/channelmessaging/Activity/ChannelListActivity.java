package alex.chopard.channelmessaging.Activity;

import android.app.ActionBar;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import alex.chopard.channelmessaging.Adapter.AdapterChannel;
import alex.chopard.channelmessaging.Class.Channel;
import alex.chopard.channelmessaging.Class.Channels;
import alex.chopard.channelmessaging.AsyncTask.HttpPostHandler;
import alex.chopard.channelmessaging.Fragment.ChannelListFragment;
import alex.chopard.channelmessaging.Fragment.MessagesFragment;
import alex.chopard.channelmessaging.Interface.OnDownloadListener;
import alex.chopard.channelmessaging.R;
import static alex.chopard.channelmessaging.Activity.ChannelActivity.fileSendPhoto;
import static alex.chopard.channelmessaging.Activity.ChannelActivity.fileDir;
import static alex.chopard.channelmessaging.Activity.ChannelActivity.fileSendSound;

import static alex.chopard.channelmessaging.Activity.LoginActivity.PREF_TOKEN;


/**
 * Created by AlexChop on 19/01/2018.
 */

public class ChannelListActivity extends GPSActivity implements AdapterView.OnItemClickListener, View.OnClickListener{

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channellist);

        fileDir = getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getPath();
        fileSendPhoto = Environment.getExternalStorageDirectory() + "/imageS/image.jpg";
        fileSendSound = Environment.getExternalStorageDirectory() + "/son/son.jpg";
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        int idView = view.getId();
        MessagesFragment frag = (MessagesFragment) getFragmentManager().findFragmentById(R.id.fragment_messages_list);

        // On regarde si le MessageFragment est présent,
        // si c'est le cas on met à jour l'id du channel à afficher,
        // sinon on lance l'activité pour afficher les message d'un channel.
        if(frag == null){
            Intent intent = new Intent(ChannelListActivity.this, ChannelActivity.class);
            intent.putExtra("idChannel", idView);

            startActivity(intent);
        }else{
            ChannelActivity.idChannel = idView;
        }

    }

    // Quand on clique sur le bouton pour afficher la list de ses amis.
    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_afficher_list_amis){
            Intent intent = new Intent(ChannelListActivity.this, ListAmisActivity.class);
            startActivity(intent);
        }
    }
}
