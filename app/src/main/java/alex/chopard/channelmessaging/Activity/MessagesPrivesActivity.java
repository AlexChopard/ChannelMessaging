package alex.chopard.channelmessaging.Activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import alex.chopard.channelmessaging.Adapter.AdapterMessages;
import alex.chopard.channelmessaging.AsyncTask.HttpPostHandler;
import alex.chopard.channelmessaging.Class.Message;
import alex.chopard.channelmessaging.Class.Messages;
import alex.chopard.channelmessaging.Class.ResponseLogin;
import alex.chopard.channelmessaging.Interface.OnDownloadListener;
import alex.chopard.channelmessaging.R;

import static alex.chopard.channelmessaging.Activity.LoginActivity.PREF_TOKEN;

/**
 * Created by AlexChop on 05/02/2018.
 */

public class MessagesPrivesActivity extends AppCompatActivity implements View.OnClickListener{

    Button btn_envoyer;
    EditText et_message;
    ListView lv_messages;
    Gson gson;
    String idUser;
    ArrayList<Message> messages;
    SharedPreferences setting;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messagesprivee);

        idUser = getIntent().getStringExtra("idUser");
        gson = new Gson();
        setting = getSharedPreferences(PREF_TOKEN, 0);

        btn_envoyer = findViewById(R.id.btn_sendmessage);
        et_message = findViewById(R.id.et_sendmessage);
        lv_messages = findViewById(R.id.lv_message);

        btn_envoyer.setOnClickListener(this);



        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                HashMap<String, String > hm = new HashMap<>();
                hm.put("function", "getmessages");
                hm.put("accesstoken", setting.getString("accesstoken", "null"));
                hm.put("userid", idUser);

                HttpPostHandler requet = new HttpPostHandler();

                requet.addOnDownloadListener(new OnDownloadListener(){
                    @Override
                    public void onDownloadComplete(String res) {
                        //Log.i("res", res);

                        Messages reponse = gson.fromJson(res, Messages.class);
                        messages = reponse.getMessages();

                        List<HashMap<String, String>> liste = new ArrayList<>();
                        HashMap<String, String> element;

                        for(int i = 0; i < messages.size(); i++){
                            element = new HashMap<>();
                            element.put("userName", messages.get(i).getUsername());
                            element.put("message", messages.get(i).getMessage());
                            element.put("date", messages.get(i).getDate());
                            element.put("imageURL", messages.get(i).getImageUrl());
                            element.put("sendbyme", String.valueOf(messages.get(i).getSendbyme()));
                            element.put("everRead", messages.get(i).getEverRead());
                            liste.add(element);
                        }

                        try{
                            AdapterMessages adapter = new AdapterMessages(MessagesPrivesActivity.this, liste);

                            lv_messages.setAdapter(adapter);
                        }catch (Exception ex){
                            Log.i("err", ex.getMessage());
                        }

                    }

                    @Override
                    public void onDownloadError(String error) {

                    }
                });

                requet.execute(hm);
            }
        },100,10000);

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_sendmessage){
            HashMap<String, String > hm = new HashMap<>();

            hm.put("function", "sendmessage");
            hm.put("accesstoken", setting.getString("accesstoken", "null"));
            hm.put("userid", idUser);
            hm.put("message", et_message.getText().toString());

            HttpPostHandler requet = new HttpPostHandler();

            requet.addOnDownloadListener(new OnDownloadListener(){
                @Override
                public void onDownloadComplete(String res) {
                    //Log.i("res", res);

                    ResponseLogin reponse = gson.fromJson(res, ResponseLogin.class);
                    if(reponse.getCode().equals("200")){
                        et_message.setText("");
                        Toast.makeText(getApplicationContext(), "send", Toast.LENGTH_SHORT).show();

                    } else{
                        Toast.makeText(getApplicationContext(), "error!", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onDownloadError(String error) {

                }
            });


            requet.execute(hm);
        }
    }
}
