package alex.chopard.channelmessaging.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import alex.chopard.channelmessaging.Adapter.AdapterChannel;
import alex.chopard.channelmessaging.Adapter.AdapterFriends;
import alex.chopard.channelmessaging.Class.User;
import alex.chopard.channelmessaging.R;
import alex.chopard.channelmessaging.UserDataSource;

/**
 * Created by AlexChop on 29/01/2018.
 */

public class ListAmisActivity extends AppCompatActivity implements GridView.OnItemClickListener{

    GridView gv_frends;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listamis);

        gv_frends = findViewById(R.id.gv_friends);

        // Création datasource pour accédé à la bdd
        UserDataSource uds = new UserDataSource(this);
        uds.open();
        // On récupère tout nos amis qui sont dans la BDD
        List<User> users = uds.getAllUsers();

        List<HashMap<String, String>> liste = new ArrayList<>();
        HashMap<String, String> element;

        for(int i = 0; i < users.size(); i++){
            element = new HashMap<>();
            element.put("id", users.get(i).getUserId() + "");
            element.put("name", users.get(i).getUsername());
            liste.add(element);
        }

        // Création de notre adapter
        AdapterFriends adapter = new AdapterFriends(ListAmisActivity.this, liste);
        gv_frends.setAdapter(adapter);

        // Fermeture de la conneion vers la BDD
        uds.close();

        gv_frends.setOnItemClickListener(this);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(parent.getId() == R.id.gv_friends){
            Intent intent = new Intent(ListAmisActivity.this, MessagesPrivesActivity.class);

            TextView idUser = view.findViewById(R.id.tv_idUser);
            intent.putExtra("idUser", idUser.getText().toString());

            startActivity(intent);
        }

    }
}
