package alex.chopard.channelmessaging.Activity;

import android.app.FragmentManager;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;

import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;

import alex.chopard.channelmessaging.Class.Message;
import alex.chopard.channelmessaging.R;

import static alex.chopard.channelmessaging.Activity.LoginActivity.PREF_TOKEN;

/**
 * Created by AlexChop on 19/01/2018.
 */

public class ChannelActivity extends GPSActivity {

    public static String fileDir;
    public static int idChannel = -1;
    public static String fileSendPhoto, fileSendSound;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channel);

        idChannel = getIntent().getIntExtra("idChannel", -1);
    }
}
