package alex.chopard.channelmessaging.AsyncTask;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import alex.chopard.channelmessaging.Interface.OnDownloadListener;

import static alex.chopard.channelmessaging.Activity.ChannelActivity.fileDir;

/**
 * Created by AlexChop on 22/01/2018.
 */

public class LoadPicture extends AsyncTask<String, Integer, Boolean> {

    public static final String pictureFormat = ".jpg";

    private ArrayList<OnDownloadListener> listeners = new ArrayList<>();


    public void  addOnDownloadListener(OnDownloadListener listener) {
        // Store the listener object
        this.listeners.add(listener);
    }


    @Override
    protected Boolean doInBackground(String... strings) {
        try{
            String name = strings[0];
            String url =  strings[1];
            String fileName = fileDir + "/" + name + pictureFormat;

            return downloadFromUrl(url, fileName);
        }catch (Exception ex){
            Log.i("erreur", ex.getMessage());
            return false;
        }

    }

    @Override
    protected void onPostExecute(Boolean res) {
        super.onPostExecute(res);
        for (OnDownloadListener oneListener: listeners) {
            if(res) {
                oneListener.onDownloadComplete("OK");
            }
            else
                oneListener.onDownloadError("!OK");
        }
    }

    public boolean downloadFromUrl(String fileURL, String fileName) {
        try{
            URL url = new URL(fileURL);
            /* Open a connection to that URL. */
            URLConnection ucon = url.openConnection();
            /* Define Input Streams to read from the URL Connection.*/
            InputStream is = ucon.getInputStream();

            File file = new File(fileName);
            file.createNewFile();

            /* Read bytes to the Buffer until the reis nothing more to read(-1) and writeon the flyin the file.*/
            FileOutputStream fos = new FileOutputStream(file);
            final int BUFFER_SIZE = 23 * 1024;
            BufferedInputStream bis = new BufferedInputStream(is, BUFFER_SIZE);
            byte[] baf = new byte[BUFFER_SIZE];
            int actual= 0;
            while(actual != -1) {
                fos.write(baf, 0, actual);
                actual = bis.read(baf, 0, BUFFER_SIZE);
            }
            fos.close();
            is.close();
            return true;
        } catch (IOException e) { //TODO HANDLER } }
            Log.i("erreurLoad", e.getMessage());
            return false;
        }
    }
}
