package alex.chopard.channelmessaging.AsyncTask;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import alex.chopard.channelmessaging.Interface.OnDownloadListener;

/**
 * Created by AlexChop on 19/01/2018.
 */

public class HttpPostHandler extends AsyncTask<HashMap<String, String>, Integer, String> {

    private ArrayList<OnDownloadListener> listeners = new ArrayList<>();


    public void  addOnDownloadListener(OnDownloadListener listener) {
        // Store the listener object
        this.listeners.add(listener);
    }

    @Override
    protected String doInBackground(HashMap<String, String>... arg0) {
        String result = null;
        try{
            result = performPostCall("http://www.raphaelbischof.fr/messaging/?function=" + arg0[0].get("function"), arg0[0]);

            return result;
        }catch (Exception e){
            Log.i("error", e.getMessage());
            return result;
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        for (OnDownloadListener oneListener: listeners) {
            if(!s.equals(""))
                oneListener.onDownloadComplete(s);
            else
                oneListener.onDownloadError(s);
        }
    }

    public String performPostCall(String requestURL, HashMap<String, String> postDataParams) {
        URL url;
        String response = "";
        try{
            url = new URL(requestURL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer= new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(postDataParams));
            writer.flush();
            writer.close();
            os.close();
            int responseCode = conn.getResponseCode();
            if (responseCode== HttpsURLConnection.HTTP_OK) {
                String line; BufferedReader br =new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while((line = br.readLine()) != null) {
                    response += line;
                }
            } else{
                response = "";
            }
        } catch (Exception e) {
            // En cas d'erreur de connection (pas d'internet) on redirige vers le listener d'erreur.
            /*for (OnDownloadListener oneListener: listeners) {
                oneListener.onDownloadError(e.getMessage());
            }*/

            e.printStackTrace();
        }

        return response;
    }

    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");
            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }
        return result.toString();
    }

}
