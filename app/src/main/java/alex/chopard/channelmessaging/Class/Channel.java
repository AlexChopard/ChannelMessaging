package alex.chopard.channelmessaging.Class;

/**
 * Created by AlexChop on 19/01/2018.
 */

public class Channel {
    private String channelID;
    private String name;
    private String connectedusers;

    public Channel(String channelID, String name, String connectedusers){
        this.setChannelID(channelID);
        this.setName(name);
        this.setConnectedusers(connectedusers);
    }

    public String getChannelID() {
        return channelID;
    }

    public String getName() {
        return name;
    }

    public String getConnectedusers() {
        return connectedusers;
    }

    public void setChannelID(String channelID) {
        this.channelID = channelID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setConnectedusers(String connectedusers) {
        this.connectedusers = connectedusers;
    }
}
