package alex.chopard.channelmessaging.Class;

import java.util.UUID;

/**
 * Created by AlexChop on 29/01/2018.
 */

public class User {

    private int userId;
    private String username;
    private String imageUrl;

    public User(){

    }

    public User(int userId, String username, String imageUrl) {
        this.userId = userId;
        this.username = username;
        this.imageUrl = imageUrl;
    }


    @Override
    public String toString() {
        return "Name : " + this.getUsername();
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
