package alex.chopard.channelmessaging.Class;

/**
 * Created by AlexChop on 19/01/2018.
 */

public class ResponseLogin {
    private String response;
    private String code;
    private String accesstoken;


    public ResponseLogin(String response, String code, String accesstoken){
        this.setResponse(response);
        this.setCode(code);
        this.setAccesstoken(accesstoken);
    }


    public String getCode() {
        return code;
    }

    public String getAccesstoken() {
        return accesstoken;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setAccesstoken(String accesstoken) {
        this.accesstoken = accesstoken;
    }
}
