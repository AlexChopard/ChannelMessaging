package alex.chopard.channelmessaging.Class;

import java.util.ArrayList;

import alex.chopard.channelmessaging.Class.Channel;

/**
 * Created by AlexChop on 19/01/2018.
 */

public class Channels {
    private ArrayList<Channel> channels;

    public Channels(ArrayList<Channel> channels){
        this.setChannels(channels);
    }

    public ArrayList<Channel> getChannels() {
        return channels;
    }

    public void setChannels(ArrayList<Channel> channels) {
        this.channels = channels;
    }

}
