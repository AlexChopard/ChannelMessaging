package alex.chopard.channelmessaging.Class;

import java.util.ArrayList;

/**
 * Created by AlexChop on 21/01/2018.
 */

public class Messages {
    private ArrayList<Message> messages;

    public Messages(ArrayList<Message> messages){
        this.setMessages(messages);
    }

    public ArrayList<Message> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<Message> messages) {
        this.messages = messages;
    }
}
