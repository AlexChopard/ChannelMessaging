package alex.chopard.channelmessaging.Fragment;

import android.app.FragmentManager;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import alex.chopard.channelmessaging.Activity.GPSActivity;
import alex.chopard.channelmessaging.Adapter.AdapterMessages;
import alex.chopard.channelmessaging.Fragment.DialogFragment.AjouterUnAmiDialogFragment;
import alex.chopard.channelmessaging.AsyncTask.HttpPostHandler;
import alex.chopard.channelmessaging.AsyncTask.UploadFileToServer;
import alex.chopard.channelmessaging.Class.Message;
import alex.chopard.channelmessaging.Class.Messages;
import alex.chopard.channelmessaging.Class.ResponseLogin;
import alex.chopard.channelmessaging.Fragment.DialogFragment.SoundRecordDialog;
import alex.chopard.channelmessaging.Interface.OnDownloadListener;
import alex.chopard.channelmessaging.R;

import static alex.chopard.channelmessaging.Activity.ChannelActivity.idChannel;
import static alex.chopard.channelmessaging.Activity.LoginActivity.PREF_TOKEN;
import static android.app.Activity.RESULT_OK;

/**
 * Created by AlexChop on 26/02/2018.
 */

public class MessagesFragment extends Fragment implements UploadFileToServer.OnUploadFileListener, View.OnClickListener, AdapterView.OnItemClickListener {

    final int PICTURE_REQUEST_CODE = 1;

    Gson gson;
    ListView lv_messages;
    EditText et_message;
    Button btn_envoyer, btn_sendphoto, btn_sendsound;
    SharedPreferences setting;
    ArrayList<Message> messages;
    FragmentManager fm;
    String fileName;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_messages, container);

        lv_messages = v.findViewById(R.id.lv_message);
        et_message = v.findViewById(R.id.et_sendmessage);
        btn_envoyer = v.findViewById(R.id.btn_sendmessage);
        btn_sendphoto = v.findViewById(R.id.btn_sendphoto);
        btn_sendsound = v.findViewById(R.id.btn_sendsound);

        return  v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        fm = getFragmentManager();

        btn_envoyer.setOnClickListener(this);
        btn_sendphoto.setOnClickListener(this);
        btn_sendsound.setOnClickListener(this);
        lv_messages.setOnItemClickListener(this);

        setting = this.getActivity().getSharedPreferences(PREF_TOKEN, 0);
        gson = new Gson();




        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if(idChannel != -1){
                    HashMap<String, String > hm = new HashMap<>();
                    hm.put("function", "getmessages");
                    hm.put("accesstoken", setting.getString("accesstoken", "null"));
                    hm.put("channelid", String.valueOf(idChannel));

                    HttpPostHandler requet = new HttpPostHandler();

                    requet.addOnDownloadListener(new OnDownloadListener() {
                        @Override
                        public void onDownloadComplete(String res) {
                            //Log.i("res", res);

                            if(getActivity() != null){
                                Messages reponse = gson.fromJson(res, Messages.class);
                                messages = reponse.getMessages();

                                List<HashMap<String, String>> liste = new ArrayList<>();
                                HashMap<String, String> element;

                                for(int i = 0; i < messages.size(); i++){
                                    element = new HashMap<>();
                                    element.put("userName", messages.get(i).getUsername());
                                    element.put("message", messages.get(i).getMessage());
                                    element.put("date", messages.get(i).getDate());
                                    element.put("imageURL", messages.get(i).getImageUrl());
                                    element.put("messageImageUrl", messages.get(i).getMessageImageUrl());
                                    element.put("soundUrl", messages.get(i).getSoundUrl());
                                    liste.add(element);
                                }

                                AdapterMessages adapter = new AdapterMessages(getActivity(), liste);

                                lv_messages.setAdapter(adapter);
                            }
                        }

                        @Override
                        public void onDownloadError(String error) {

                        }
                    });


                    requet.execute(hm);
                }
            }
        },100,10000);

    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btn_sendmessage){

            HashMap<String, String > hm = new HashMap<>();

            hm.put("function", "sendmessage");
            hm.put("accesstoken", setting.getString("accesstoken", "null"));
            hm.put("channelid", String.valueOf(idChannel));
            hm.put("message", et_message.getText().toString());

            GPSActivity activity = (GPSActivity) this.getActivity();
            Location location = activity.getmCurrentLocation();

            // Si la position n'as pas pu être chargé, on met les valeurs par default
            hm.put("latitude", String.valueOf((location != null) ? location.getLatitude(): 0));
            hm.put("longitude", String.valueOf((location != null) ? location.getLongitude(): 0));


            HttpPostHandler requet = new HttpPostHandler();

            requet.addOnDownloadListener(new OnDownloadListener(){
                @Override
                public void onDownloadComplete(String res) {
                    //Log.i("res", res);

                    ResponseLogin reponse = gson.fromJson(res, ResponseLogin.class);
                    if(reponse.getCode().equals("200")){
                        et_message.setText("");
                        Toast.makeText(getActivity(), "send", Toast.LENGTH_SHORT).show();

                    } else{
                        Toast.makeText(getActivity(), "error", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onDownloadError(String error) {

                }
            });

            requet.execute(hm);
        }else if(view.getId() == R.id.btn_sendphoto){

            try{
                File dir = new File(Environment.getExternalStorageDirectory() + "/tmpImg/");
                dir.mkdir();

                File photo = new File(dir, "photo.jpg");
                fileName = photo.getAbsolutePath();
                Uri uri = Uri.fromFile(photo);

                Log.i("test", fileName);

                //Création de l’appel à l’application appareil photo pour récupérer une image
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uri); //Emplacement de l’image stockée
                startActivityForResult(intent, PICTURE_REQUEST_CODE);
            }catch (Exception ex){
                Log.i("error", ex.getMessage());
            }
        } else if(view.getId() == R.id.btn_sendsound){
            SoundRecordDialog dialog = new SoundRecordDialog();

            dialog.show(fm, "Son");
        }


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        AjouterUnAmiDialogFragment dialog = new AjouterUnAmiDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("userid", messages.get(position).getUserID());
        bundle.putString("username", messages.get(position).getUsername());
        bundle.putString("imageurl", messages.get(position).getImageUrl());
        bundle.putString("latitude", messages.get(position).getLatitude());
        bundle.putString("longitude", messages.get(position).getLongitude());

        dialog.setArguments(bundle);

        dialog.show(fm, id + "");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case PICTURE_REQUEST_CODE:
                if(resultCode == RESULT_OK){

                    try{
                        File photoFile = new File(fileName);
                        resizeFile(photoFile, this.getActivity());

                        List<NameValuePair> values = new ArrayList<>();
                        values.add(new BasicNameValuePair("accesstoken", setting.getString("accesstoken", "null")));
                        values.add(new BasicNameValuePair("channelid", String.valueOf(idChannel)));

                        GPSActivity activity = (GPSActivity) this.getActivity();
                        Location location = activity.getmCurrentLocation();

                        // Si la position n'as pas pu être chargé, on met les valeurs par default
                        values.add(new BasicNameValuePair("latitude", String.valueOf((location != null) ? location.getLatitude(): 0)));
                        values.add(new BasicNameValuePair("longitude", String.valueOf((location != null) ? location.getLongitude(): 0)));


                        UploadFileToServer upload = new UploadFileToServer(getActivity(), fileName, values, this);

                        upload.execute();

                    }catch (Exception e){
                        Log.i("error", e.getMessage());
                    }

                }

                break;
        }
    }

    @Override
    public void onResponse(String result){
        Log.i("res", result);
        Toast.makeText(getActivity(), "Photo envoyée", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailed(IOException error){
        Log.i("failed!", error.getMessage());
        Toast.makeText(getActivity(), "Erreur lors de l'envoi", Toast.LENGTH_SHORT).show();
    }





    //decodes image and scales it to reduce memory consumption
    private void resizeFile(File f, Context context) throws IOException {
        //Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(new FileInputStream(f),null,o);

        //The new size we want to scale to
        final int REQUIRED_SIZE=400;

        //Find the correct scale value. It should be the power of 2.
        int scale=1;
        while(o.outWidth/scale/2>=REQUIRED_SIZE && o.outHeight/scale/2>=REQUIRED_SIZE)
            scale*=2;

        //Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize=scale;
        Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        int i = getCameraPhotoOrientation(context, Uri.fromFile(f),f.getAbsolutePath());
        if (o.outWidth>o.outHeight)
        {
            Matrix matrix = new Matrix();
            matrix.postRotate(i); // anti-clockwise by 90 degrees
            bitmap = Bitmap.createBitmap(bitmap , 0, 0, bitmap .getWidth(), bitmap .getHeight(), matrix, true);
        }
        try {
            f.delete();
            FileOutputStream out = new FileOutputStream(f);
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int getCameraPhotoOrientation(Context context, Uri imageUri, String imagePath) throws IOException {
        int rotate = 0;
        context.getContentResolver().notifyChange(imageUri, null);
        File imageFile = new File(imagePath);
        ExifInterface exif = new ExifInterface(
                imageFile.getAbsolutePath());
        int orientation = exif.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_270:
                rotate = 270;
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                rotate = 180;
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                rotate = 90;
                break;
        }
        return rotate;
    }
}

