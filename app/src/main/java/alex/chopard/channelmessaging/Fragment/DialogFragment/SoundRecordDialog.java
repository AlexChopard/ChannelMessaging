package alex.chopard.channelmessaging.Fragment.DialogFragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import alex.chopard.channelmessaging.Activity.GPSActivity;
import alex.chopard.channelmessaging.Activity.MapActivity;
import alex.chopard.channelmessaging.AsyncTask.UploadFileToServer;
import alex.chopard.channelmessaging.Class.User;
import alex.chopard.channelmessaging.R;
import alex.chopard.channelmessaging.UserDataSource;

import static alex.chopard.channelmessaging.Activity.ChannelActivity.fileSendSound;
import static alex.chopard.channelmessaging.Activity.ChannelActivity.idChannel;
import static alex.chopard.channelmessaging.Activity.LoginActivity.PREF_TOKEN;


/**
 * Created by root on 3/7/18.
 */

public class SoundRecordDialog extends DialogFragment{

    String[] arr = {"Lancer l'enregistrement", "Stoper l'enregistrement", "Jouer le son", "Stoper le son"};
    MediaRecorder mRecorder = null;
    MediaPlayer mPlayer = null;
    String mFileName;
    SharedPreferences setting;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        mFileName = getActivity().getExternalCacheDir().getAbsolutePath();
        mFileName += "/son.3gp";
        setting = this.getActivity().getSharedPreferences(PREF_TOKEN, 0);

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        Dialog dialog = builder .setTitle(R.string.recordingSound)
                .setItems(arr, null)
                .setNegativeButton(R.string.recordSend, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Envoyer le son au serveur
                        try{
                            File sonFile = new File(mFileName);

                            List<NameValuePair> values = new ArrayList<>();
                            values.add(new BasicNameValuePair("accesstoken", setting.getString("accesstoken", "null")));
                            values.add(new BasicNameValuePair("channelid", String.valueOf(idChannel)));

                            GPSActivity activity = (GPSActivity) getActivity();
                            Location location = activity.getmCurrentLocation();

                            // Si la position n'as pas pu être chargé, on met les valeurs par default
                            values.add(new BasicNameValuePair("latitude", String.valueOf((location != null) ? location.getLatitude(): 0)));
                            values.add(new BasicNameValuePair("longitude", String.valueOf((location != null) ? location.getLongitude(): 0)));


                            UploadFileToServer upload = new UploadFileToServer(getActivity(), mFileName, values, new UploadFileToServer.OnUploadFileListener() {
                                @Override
                                public void onResponse(String result){
                                    Log.i("res", result);
                                    //Toast.makeText(getActivity(), "Son envoyée", Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onFailed(IOException error){
                                    Log.i("failed!", error.getMessage());
                                    //Toast.makeText(getActivity(), "Erreur lors de l'envoi", Toast.LENGTH_SHORT).show();
                                }
                            });

                            upload.execute();

                        }catch (Exception e){
                            Log.i("error", e.getMessage());
                        }
                    }
                })
                .setPositiveButton(R.string.recordCancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Annuler
                    }
                }).create()
                ;

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(final DialogInterface dialog) {

                ListView lv = ((AlertDialog) dialog).getListView();

                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        switch (position){
                            case 0: // Start record
                                mRecorder = new MediaRecorder();
                                mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                                mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                                mRecorder.setOutputFile(mFileName);
                                mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

                                try {
                                    mRecorder.prepare();
                                } catch (IOException e) {
                                    Log.e("error", e.getMessage());
                                }

                                mRecorder.start();

                                break;
                            case 1: // Stop recording
                                mRecorder.stop();
                                mRecorder.release();
                                mRecorder = null;
                                break;
                            case 2: // Start playing
                                mPlayer = new MediaPlayer();
                                try {
                                    mPlayer.setDataSource(mFileName);
                                    mPlayer.prepare();
                                    mPlayer.start();
                                } catch (IOException e) {
                                    Log.e("error", "prepare() failed");
                                }

                                break;
                            case 3: // Stop palying
                                mPlayer.release();
                                mPlayer = null;
                                break;
                        }
                    }
                });
            }
        });
        return dialog;
    }



}
