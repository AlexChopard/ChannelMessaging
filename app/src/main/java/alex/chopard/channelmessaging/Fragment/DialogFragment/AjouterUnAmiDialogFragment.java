package alex.chopard.channelmessaging.Fragment.DialogFragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;

import alex.chopard.channelmessaging.Activity.ChannelActivity;
import alex.chopard.channelmessaging.Activity.MapActivity;
import alex.chopard.channelmessaging.Class.User;
import alex.chopard.channelmessaging.R;
import alex.chopard.channelmessaging.UserDataSource;

/**
 * Created by AlexChop on 29/01/2018.
 */

public class AjouterUnAmiDialogFragment extends DialogFragment{

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        String[] arr = {"Ajouter un ami", "Voir sur la carte"};

        builder .setTitle(R.string.addfriend)
                .setItems(arr, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //which = la position de l'item appuyé
                        if (which == 0) {
                            Bundle bundle = getArguments();

                            int userId = bundle.getInt("userid");
                            String username = bundle.getString("username");
                            String imageUrl = bundle.getString("imageurl");

                            Log.i("user", username + " " + imageUrl);

                            UserDataSource uds = new UserDataSource(getActivity());
                            uds.open();
                            User user = uds.createFriend(userId, username, imageUrl);
                            Log.i("user", user.toString());
                            uds.close();
                        } else {
                            //Do some over stuff (2nd item touched)

                            Bundle bundle = getArguments();

                            String latitude = bundle.getString("latitude");
                            String longitude = bundle.getString("longitude");
                            String username = bundle.getString("username");

                            Intent intent = new Intent(getActivity(), MapActivity.class);
                            intent.putExtra("latitude", latitude);
                            intent.putExtra("longitude", longitude);
                            intent.putExtra("name", username);
                            startActivity(intent);
                        }
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }

}
