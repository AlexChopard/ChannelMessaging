package alex.chopard.channelmessaging.Fragment;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import alex.chopard.channelmessaging.Activity.ChannelListActivity;
import alex.chopard.channelmessaging.Adapter.AdapterChannel;
import alex.chopard.channelmessaging.AsyncTask.HttpPostHandler;
import alex.chopard.channelmessaging.Class.Channel;
import alex.chopard.channelmessaging.Class.Channels;
import alex.chopard.channelmessaging.Interface.OnDownloadListener;
import alex.chopard.channelmessaging.R;

import static alex.chopard.channelmessaging.Activity.LoginActivity.PREF_TOKEN;

/**
 * Created by AlexChop on 26/02/2018.
 */

public class ChannelListFragment extends Fragment implements OnDownloadListener {

    ListView lv_channel;
    Gson gson;
    Button btn_amis;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_channel, container);
        lv_channel = v.findViewById(R.id.lv_channel);
        btn_amis = v.findViewById(R.id.btn_afficher_list_amis);

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        HashMap<String, String > hm = new HashMap<>();
        HttpPostHandler requet = new HttpPostHandler();

        gson = new Gson();

        SharedPreferences setting = this.getActivity().getSharedPreferences(PREF_TOKEN, 0);
        hm.put("function", "getchannels");
        hm.put("accesstoken", setting.getString("accesstoken", "null"));

        requet.addOnDownloadListener(this);
        lv_channel.setOnItemClickListener((ChannelListActivity) getActivity());
        btn_amis.setOnClickListener((ChannelListActivity) getActivity());

        requet.execute(hm);
    }


    @Override
    public void onDownloadComplete(String res) {
        //Log.i("res", res);

        ChannelListFragment frag = (ChannelListFragment) getFragmentManager().findFragmentById(R.id.fragment_channel_list);

        Channels reponse = gson.fromJson(res, Channels.class);
        ArrayList<Channel> lesChannels = reponse.getChannels();

        List<HashMap<String, String>> liste = new ArrayList<>();
        HashMap<String, String> element;

        for(int i = 0; i < lesChannels.size(); i++){
            element = new HashMap<>();
            element.put("id", lesChannels.get(i).getChannelID());
            element.put("name", lesChannels.get(i).getName());
            element.put("number", "Nombre d'utilisateur connectés : " + lesChannels.get(i).getConnectedusers());
            liste.add(element);
        }

        AdapterChannel adapter = new AdapterChannel(getActivity(), liste);

        lv_channel.setAdapter(adapter);
    }

    @Override
    public void onDownloadError(String error) {

    }
}
