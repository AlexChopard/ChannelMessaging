package alex.chopard.channelmessaging;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import alex.chopard.channelmessaging.Class.User;

/**
 * Created by AlexChop on 29/01/2018.
 */

public class UserDataSource {

    // Database fields
    private SQLiteDatabase database;
    private FriendsDB dbHelper;
    private String[] allColumns = { FriendsDB.KEY_USER_ID, FriendsDB.KEY_USERNAME, FriendsDB.KEY_IMAGE_URL };

    public UserDataSource(Context context) {
        dbHelper = new FriendsDB(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }


    public User createFriend(int userId, String username, String imageUrl) {
        ContentValues values = new ContentValues();
        values.put(FriendsDB.KEY_USERNAME, username);
        values.put(FriendsDB.KEY_IMAGE_URL, imageUrl);
        values.put(FriendsDB.KEY_USER_ID, userId);

        database.insert(FriendsDB.USER_TABLE_NAME, null, values);
        Cursor cursor = database.query(FriendsDB.USER_TABLE_NAME, allColumns, FriendsDB.KEY_USER_ID + " = \"" + userId +"\"", null, null, null, null);
        cursor.moveToFirst();
        User newUser = cursorToUser(cursor);
        cursor.close();

        return newUser;
    }

    public List<User> getAllUsers() {
        List<User> lesUsers = new ArrayList<User>();
        Cursor cursor = database.query(FriendsDB.USER_TABLE_NAME, allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            User unUser = cursorToUser(cursor);
            lesUsers.add(unUser);
            cursor.moveToNext();
        } // makesure to close the cursor
        cursor.close();
        return lesUsers;
    }


    private User cursorToUser(Cursor cursor) {
        User comment = new User();
        comment.setUserId(cursor.getInt(0));
        comment.setUsername(cursor.getString(1));
        comment.setImageUrl(cursor.getString(2));

        return comment;
    }

}
